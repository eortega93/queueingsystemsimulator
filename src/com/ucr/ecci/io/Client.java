/**
 * Universidad de Costa Rica
 * Investigacion de Operaciones
 * Queueing System Simulator
 * Created on May 7, 2018
 * @author Esteban Ortega Acuna
 * @author Jose Valverde Jara
 *
 **/


package com.ucr.ecci.io;

public class Client {
    /**
     *  Represents the arrive time of the Client
     **/
    private long arriveTime;

    /**
     *  Represents the departure time of the Client
     **/
    private long departureTime;

    /**
     *  Represents the queue time of the Client
     **/
    private long queueTime;

    /**
     *  Represents the service time of the Client
     **/
    private long serviceTime;

    public Client(long arriveTime) {
        this.arriveTime = arriveTime;
        this.departureTime = 0;
        this.queueTime = 0;
        this.serviceTime = 0;
    }

    /**
     *  Gets the arrive time of the Client
     * @return arriveTime
     **/
    public long getArriveTime() {
        return arriveTime;
    }

    /**
     *  Gets the departgetArriveTimeure time of the Client
     *  @return departureTime
     **/
    public long getDepartureTime() {
        return departureTime;
    }

    /**
     *  Gets the queue time of the Client
     *  @return queueTime
     **/
    public long getQueueTime() {
        return queueTime;
    }

    /**
     *  Gets the service time of the Client
     *  @return serviceTime
     **/
    public long getServiceTime() {
        return serviceTime;
    }

    /**
     *  Sets the arrive time of the Client
     * @param  arriveTime of the Client
     **/
    public void setArriveTime(long arriveTime) {
        this.arriveTime = arriveTime;
    }


    /**
     *  Sets the departure time of the Client
     * @param  departureTime of the Client
     **/
    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    /**
     *  Sets the queue time of the Client
     * @param queueTime of the Client
     **/
    public void setQueueTime(long queueTime) {
        this.queueTime = queueTime;
    }

    /**
     *  Sets the service time of the Client
     * @param serviceTime of the Client
     **/
    public void setServiceTime(long serviceTime) {
        this.serviceTime = serviceTime;
    }

}
