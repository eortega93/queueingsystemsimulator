/**
 * Universidad de Costa Rica
 * Investigacion de Operaciones
 * Queueing System Simulator
 * Created on May 7, 2018
 * @author Esteban Ortega Acuna
 * @author Jose Valverde Jara
 *
 **/


package com.ucr.ecci.io;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Deque;
import java.util.Stack;
import java.util.Comparator;
import java.util.Collections;
import java.util.Random;

import static com.ucr.ecci.io.Event.Type.*;

public class System {

    /**
     * This class is used to define the priority of the events
     **/
    private class EventComparator implements  Comparator<Event> {

        /**
         * This method indicates if an event is attended first than another
         * @return -1 if the first event has a higher priority,
         * returns 1 if Event e has higher priority or returns 0 if both have the same priority.
         **/
        @Override
        public int compare(Event e1, Event e2) {
            return e1.compareTo(e2);
        }
    }

    /**
     * Indicates the system clock in which calls are attended
     **/
    private long clock;

    /**
     * Indicates the system clock in which calls are attended
     **/
    private int departureClients;

    /**
     * Indicates the system clock in which calls are attended
     **/
    private int serversQuantity;

    /**
     * Indicates the system clock in which calls are attended
     **/
    private List<Event> eventList;

    /**
     * List used to handle customers who are waiting to be attended
     **/
    private Deque<Client> clientsInQueue;

    /**
     * List used to manage the clients that are being attended
     **/
    private Deque<Client> clientsInService;

    /**
     * List used to manage clients who have been served
     **/
    private List<Client> attendedClients;

    /**
     * Stack used to handle test arrival numbers
     **/
    private Stack<Integer> testArrivalNumbers;

    /**
     * Stack used to handle test departure numbers
     **/
    private Stack<Integer> testDeparturedNumbers;

    /**
     * List used to manage the arrival random numbers used in the execution
     **/
    private List<Float> randomArrivalNumbers;

    /**
     * List used to manage the departure random numbers used in the execution
     **/
    private List<Float> randomDepartureNumbers;

    /**
     * Constructor for System object, initialize the capacity of the system
     * @param servers represents capacity of servers of the system
     **/
    public System(int servers) {
        this.clock = 0;
        this.serversQuantity = servers;
        this.eventList = new ArrayList<>();
        this.clientsInQueue = new LinkedList<>();
        this.clientsInService = new LinkedList<>();
        this.departureClients = 0;
        this.attendedClients = new ArrayList<>();
        this.testArrivalNumbers = new Stack<>();
        this.testDeparturedNumbers = new Stack<>();
        this.randomArrivalNumbers = new ArrayList<>();
        this.randomDepartureNumbers = new ArrayList<>();

        //generateTestNumbers();
    }

    /**
     * Adds an event and sorts the event list using an event comparator
     * @param e the event
     **/
    public void addEvent(Event e){
        this.eventList.add(e);
        Collections.sort(eventList , new EventComparator());
    }

    /**
     * Returns the next event and remove it from the event list
     * @return an event
     **/
    public Event getNextEvent(){
        Event e = this.eventList.get(0);
        this.eventList.remove(e);
        return e;

    }

    /**
     * Adds a client in waiting queue
     * @param e the client
     **/
    public void addClientInQueue(Client e){
        this.clientsInQueue.add(e);
    }

    /**
     * Returns the next client in the waiting queue
     * @return a client
     **/
    public Client getClientInQueue(){
        return this.clientsInQueue.pollFirst();
    }

    /**
     * Adds a client in the service queue
     * @param e the client
     **/
    public void addClientInService(Client e){
        this.clientsInService.add(e);
    }

    /**
     * Returns the client in service and. The client is added to attended list
     * @return a client
     **/
    public Client getClientInService(){
        Client client  = this.clientsInService.pollFirst();
        addAttendedClient(client);
        ++departureClients;
        return client;
    }

    /**
     * Adds a client in the attended list
     * @param e the client
     **/
    public void addAttendedClient(Client e){
        this.attendedClients.add(e);
    }

    /**
     * Returns the number of clients that has been attended
     * @return departureClients
     **/
    public int getDepartureClients(){
        return this.departureClients;
    }

    /**
     * Get the next event in list,
     * determines if it is arrival or departure type and processes it,
     * print statistics of the event
     **/
    public void processNextEvent(){

        Event e = getNextEvent();
        clock = e.getTime();
        java.lang.System.out.println("\tSystem: Clock: " + clock);

        if(e.getType().equals(ARRIVAL)){
            java.lang.System.out.println("\tSystem: Process arrival event with time: " + e.getTime());
            processArrival(e);
        }else{
            java.lang.System.out.println("\tSystem: Process departure event with time: " + e.getTime());
            processDeparture(e);
        }
        printStatistics();

    }

    /**
     * Process an arrival event and generate the next event
     * @param e the event
     **/
    public void processArrival(Event e){

        Client client = new Client(e.getTime());
        long randomTime;
        if (clientsInService.size() >= this.serversQuantity ) {
            addClientInQueue(client);
        }else {
            addClientInService(client);
            randomTime = e.getTime() + generateRandomDepartureTime();
            addEvent(new Event(DEPARTURE, randomTime));
        }
        randomTime = e.getTime() + generateRandomArrivalTime();
        addEvent(new Event(ARRIVAL, randomTime));


    }

    /**
     * Process an arrival event and generate the next event
     * @param e the event
     **/
    public void processDeparture(Event e){

        Client client;
        long randomTime;
        if(clientsInQueue.size() > 0){
            client = getClientInQueue();
            client.setServiceTime(e.getTime());

            getClientInService();

            addClientInService(client);
            randomTime = e.getTime() + generateRandomDepartureTime();
            addEvent(new Event(DEPARTURE, randomTime));
        }else {
            client = getClientInService();
            client.setDepartureTime(e.getTime());
        }



    }

    /**
     * Generates a random arrival time using the probabilities of the provided distribution
     **/
    private long generateRandomArrivalTime(){

        //return testArrivalNumbers.pop();

        Random randomGenerator = new Random();
        float probability = randomGenerator.nextFloat();
        randomArrivalNumbers.add(probability);
        if(probability <= 0.40){
            return 1;
        }else if(probability >= 0.75){
            return 2;
        }else{
            return 3;
        }
    }

    /**
     * Generates a random departure time using the probabilities of the provided distribution
     **/
    private long generateRandomDepartureTime(){

        //return testDeparturedNumbers.pop();

        Random randomGenerator = new Random();
        float probability = randomGenerator.nextFloat();
        randomDepartureNumbers.add(probability);
        if(probability <= 0.1){
            return 2;
        }else if(probability >= 0.35){
            return 3;
        }else if(probability >= 0.75){
            return 4;
        }else if(probability >= 0.95){
            return 7;
        }else{
            return 10;
        }
    }

    /**
     * Uses the numbers of the provided random number list to test the execution
     **/
    public void generateTestNumbers(){

        this.testDeparturedNumbers.add(3);
        this.testDeparturedNumbers.add(7);
        this.testDeparturedNumbers.add(7);
        this.testDeparturedNumbers.add(4);
        this.testDeparturedNumbers.add(7);
        this.testDeparturedNumbers.add(7);
        this.testDeparturedNumbers.add(3);
        this.testDeparturedNumbers.add(4);
        this.testDeparturedNumbers.add(4);
        this.testDeparturedNumbers.add(3);
        this.testDeparturedNumbers.add(2);
        this.testDeparturedNumbers.add(4);

        this.testArrivalNumbers.add(2);
        this.testArrivalNumbers.add(3);
        this.testArrivalNumbers.add(1);
        this.testArrivalNumbers.add(3);
        this.testArrivalNumbers.add(2);
        this.testArrivalNumbers.add(3);
        this.testArrivalNumbers.add(1);
        this.testArrivalNumbers.add(2);
        this.testArrivalNumbers.add(1);
        this.testArrivalNumbers.add(1);
        this.testArrivalNumbers.add(1);
        this.testArrivalNumbers.add(3);
        this.testArrivalNumbers.add(1);
        this.testArrivalNumbers.add(3);
        this.testArrivalNumbers.add(2);
    }

    /**
     * Print the statistics of the execution
     **/
    public void printStatistics(){

        java.lang.System.out.println("\tClients being attended: " + clientsInService.size());
        java.lang.System.out.println("\tClients waiting on queue: " + clientsInQueue.size());
        java.lang.System.out.println("\tTotal number of attended clients: " + departureClients);
        java.lang.System.out.println("\tEvent list:");
        for(Event e : eventList) {
            java.lang.System.out.println("\t\tEvent type: "+e.getType() + " , time: "+ e.getTime());
        }
    }
    /**
     * Print the random numbers used in the execution
     **/
    public void printRandomNumbers(){

        java.lang.System.out.println("\nRandom arrival numbers:");
        for(Float f : randomArrivalNumbers) {
            java.lang.System.out.println("\t"+f);
        }


        java.lang.System.out.println("Random departure numbers:");
        for(Float f : randomDepartureNumbers) {
            java.lang.System.out.println("\t"+f);
        }
    }
}
