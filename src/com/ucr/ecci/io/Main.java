/**
 * Universidad de Costa Rica
 * Investigacion de Operaciones
 * Queueing System Simulator
 * Created on May 7, 2018
 * @author Esteban Ortega Acuna
 * @author Jose Valverde Jara
 *
 **/

package com.ucr.ecci.io;
public class Main {

    /**
     * This variable is used to run the simulation only for 15 clients
     **/
    private static int FINAL_CONDITION = 15;

    public static void main(String[] args) {
        Simulation simulation = new Simulation(FINAL_CONDITION);
        simulation.run();

    }
}
