/**
 * Universidad de Costa Rica
 * Investigacion de Operaciones
 * Queueing System Simulator
 * Created on May 7, 2018
 * @author Esteban Ortega Acuna
 * @author Jose Valverde Jara
 *
 **/

package com.ucr.ecci.io;

public class Event implements Comparable<Event>{

    /**
     * This enum represents the two possible types of an Event
     **/
    public enum Type  {
        /**
         * this represent new incoming call
         **/
        ARRIVAL,

        /**
         * this represent new incoming call
         **/
        DEPARTURE;
    }

    /**
     *  Represents the type of the Event
     **/
    private Type type;

    /**
     *  Represents the occurrence time of the Event
     **/
    private long time;

    /**
     * Constructor for a new Event
     * @param type represents the type of the Event
     * @param time represents the occurrence time of the Event
     **/
    public Event(Type type, long time) {
        this.type = type;
        this.time = time;
    }

    /**
     * Returns the type of the Event
     * @return the type of the Event
     **/
    public Type getType() {
        return type;
    }

    /**
     * Returns the occurrence time of the Event
     * @return time of the Event
     **/
    public long getTime() {
        return time;
    }

    /**
     * This method indicates if an event is attended first than another
     * @return -1 if the first event has a higher priority,
     * returns 1 if Event e has higher priority or returns 0 if both have the same priority.
     **/
    @Override
    public int compareTo(Event e) {
        if(this.time < e.time){
            return -1;
        }else if(this.time > e.time){
            return 1;
        }else{
            if(this.type.equals(e.type) ){
                return 0;
            }else{
                if(this.type.equals(Type.DEPARTURE)){
                    return -1;
                }else{
                    return 0;
                }
            }
        }
    }
}
