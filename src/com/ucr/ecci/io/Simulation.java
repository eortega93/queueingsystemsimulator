/**
 * Universidad de Costa Rica
 * Investigacion de Operaciones
 * Queueing System Simulator
 * Created on May 7, 2018
 * @author Esteban Ortega Acuna
 * @author Jose Valverde Jara
 *
 **/


package com.ucr.ecci.io;

import static com.ucr.ecci.io.Event.Type.*;

public class Simulation {

    /**
     * This variable is used to run the simulation only for the amount of clients that is indicated in the constructor
     **/
    private int endCondition;

    /**
     * This number is used to indicate the capacity of the system
     **/
    private int SERVER_QUANTITY = 2;

    /**
     * This variable is used to handle the system
     **/
    private System system;


    public Simulation(int endCondition) {
        if(endCondition < 0) {
            throw new IllegalStateException("Condition must be greater than 0");
        }
        this.endCondition = endCondition;
        this.system = new System(SERVER_QUANTITY);
    }

    /**
     * Generate the initial event and run the simulation as many times as indicated
     **/
    public void run(){
        java.lang.System.out.println("Simulator: Simulation begins with " + SERVER_QUANTITY + " servers, for " + endCondition + " clients.");

        Event initialEvent = new Event(ARRIVAL, 0);
        system.addEvent(initialEvent);

        while (system.getDepartureClients() < endCondition) {
            java.lang.System.out.println("\nSimulator: Process next event at time");
            system.processNextEvent();
        }
        system.printRandomNumbers();
    }
}
